import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import Sequelize, { Model, ModelAttributes, ModelIndexesOptions } from "@golemio/core/dist/shared/sequelize";
import { IRouteOutputDto } from "./interfaces/IRouteOutputDto";

export class RouteModel extends Model<IRouteOutputDto> implements IRouteOutputDto {
    public static tableName = "firebase_pidlitacka_route";

    declare count_case: number;
    declare reference_date: string;
    declare s_from: string;
    declare s_to: string;

    public static attributeModel: ModelAttributes<RouteModel> = {
        count_case: { type: Sequelize.INTEGER, allowNull: false },
        reference_date: { type: Sequelize.DATE, primaryKey: true },
        s_from: { type: Sequelize.STRING, primaryKey: true },
        s_to: { type: Sequelize.STRING, primaryKey: true },
    };

    public static indexesOptions: ModelIndexesOptions[] = [
        {
            unique: false,
            using: "BTREE",
            name: "firebase_pidlitacka_route_create_batch",
            fields: ["create_batch_id"],
        },
        {
            unique: false,
            using: "BTREE",
            name: "firebase_pidlitacka_route_update_batch",
            fields: ["update_batch_id"],
        },
    ];

    public static jsonSchema: JSONSchemaType<IRouteOutputDto[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                count_case: { type: "integer" },
                reference_date: { type: "string" },
                s_from: { type: "string" },
                s_to: { type: "string" },
            },
            required: ["count_case", "reference_date", "s_from", "s_to"],
        },
    };
}
