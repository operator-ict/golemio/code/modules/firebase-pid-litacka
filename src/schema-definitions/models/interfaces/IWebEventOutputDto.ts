export interface IWebEventOutputDto {
    id: string | null;
    id_profile: string | null;
    reference_date: string;
    sessions: number | null;
    users: number | null;
}
