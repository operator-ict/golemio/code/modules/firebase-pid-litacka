import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IAppLaunchInputDto } from "./interfaces/IAppLaunchInputDto";

export const appLaunchDtoSchema: JSONSchemaType<IAppLaunchInputDto[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            anonym: { type: "number" },
            anonym_users: { type: "number" },
            event_count: { type: "number" },
            event_date: { type: "string" },
            hasnfc: { type: "number" },
            hasnfc_users: { type: "number" },
            isanonym: { type: "number" },
            isanonym_users: { type: "number" },
            mail_users: { type: "number" },
            mails: { type: "number" },
            noti_users: { type: "number" },
            notis: { type: "number" },
            users: { type: "number" },
        },
        required: [
            "anonym",
            "anonym_users",
            "event_count",
            "event_date",
            "hasnfc",
            "hasnfc_users",
            "isanonym",
            "isanonym_users",
            "mail_users",
            "mails",
            "noti_users",
            "notis",
            "users",
        ],
    },
};
