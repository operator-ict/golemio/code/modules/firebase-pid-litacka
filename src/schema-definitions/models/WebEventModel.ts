import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import Sequelize, { Model, ModelAttributes, ModelIndexesOptions } from "@golemio/core/dist/shared/sequelize";
import { IWebEventOutputDto } from "./interfaces/IWebEventOutputDto";

export class WebEventModel extends Model<IWebEventOutputDto> implements IWebEventOutputDto {
    public static tableName = "firebase_pidlitacka_web_events";

    declare id: string | null;
    declare id_profile: string | null;
    declare reference_date: string;
    declare sessions: number | null;
    declare users: number | null;

    public static attributeModel: ModelAttributes<WebEventModel> = {
        id: { type: Sequelize.STRING },
        id_profile: { type: Sequelize.STRING },
        reference_date: { type: Sequelize.DATE, primaryKey: true },
        sessions: { type: Sequelize.INTEGER },
        users: { type: Sequelize.INTEGER },
    };

    public static indexesOptions: ModelIndexesOptions[] = [
        {
            unique: false,
            using: "BTREE",
            name: "firebase_pidlitacka_web_events_create_batch",
            fields: ["create_batch_id"],
        },
        {
            unique: false,
            using: "BTREE",
            name: "firebase_pidlitacka_web_events_update_batch",
            fields: ["update_batch_id"],
        },
    ];

    public static jsonSchema: JSONSchemaType<IWebEventOutputDto[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                id: {
                    oneOf: [{ type: "string" }, { type: "null", nullable: true }],
                },
                id_profile: {
                    oneOf: [{ type: "string" }, { type: "null", nullable: true }],
                },
                reference_date: { type: "string" },
                sessions: {
                    oneOf: [{ type: "number" }, { type: "null", nullable: true }],
                },
                users: {
                    oneOf: [{ type: "number" }, { type: "null", nullable: true }],
                },
            },
            required: ["reference_date"],
        },
    };
}
