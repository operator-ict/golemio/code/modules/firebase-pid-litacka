export interface IWebEventInputDto {
    id: string;
    id_profile: string;
    reference_date: string;
    sessions: number;
    users: number;
}
