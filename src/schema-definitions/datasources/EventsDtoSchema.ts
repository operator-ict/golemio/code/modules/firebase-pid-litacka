import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IEventInputDto } from "./interfaces/IEventInputDto";

export const eventsDtoSchema: JSONSchemaType<IEventInputDto[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            event_count: { type: "number" },
            event_date: { type: "string" },
            event_name: { type: "string" },
            users: { type: "number" },
        },
        required: ["event_count", "event_date", "event_name", "users"],
    },
};
