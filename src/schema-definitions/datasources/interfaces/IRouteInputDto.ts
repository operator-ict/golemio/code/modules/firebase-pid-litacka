export interface IRouteInputDto {
    count_case: number;
    reference_date: string;
    s_from: string;
    s_to: string;
}
