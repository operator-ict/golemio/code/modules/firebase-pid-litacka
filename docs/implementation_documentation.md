# Implementační dokumentace modulu firebase-pid-litacka


## Záměr

Modul slouží k historizaci statistik o využití aplikace PID Lítačka.


## Vstupní data

### Data aktivně stahujeme

Data přenášíme pravidelně z databáze `stage` tabulek `firebase_*`

#### Datový zdroj DB "stage"

- zdroj dat
  - url: `<POSTGRES_CONN>/stage`
  - parametry dotazu:
    - schema: `keboola`
    - tableName: `firebase_pidlitacka_applaunch_par`, `firebase_pidlitacka_events`, `firebase_pidlitacka_route`, `firebase_pidlitacka_web_events`
- formát dat
  - protokol - PostgresProtocolStrategyStreamed
  - datový typ - Buffer
  - [validační schéma](https://gitlab.com/operator-ict/golemio/code/modules/firebase-pid-litacka/src/schema-definitions/index.ts)
- frekvence stahování
  - Data se stahují jednou denně a následně jsou zmazány ze zdroje.
    ```
    {
      "cronTime": "0 15 9 * * *",
      "exchange"": "dataplatform",
      "message": "",
      "routingKey": "cron.dataplatform.firebasepidlitacka.moveAll"
    }
    ```
- název rabbitmq fronty
  - Hlavní fronta: `dataplatform.firebasepidlitacka.moveAll` spouští
    1. `dataplatform.firebasepidlitacka.moveAppLaunch`
    2. `dataplatform.firebasepidlitacka.moveEvents`
    3. `dataplatform.firebasepidlitacka.moveRoute`
    4. `dataplatform.firebasepidlitacka.moveWebEvents`,

    které nahrávají data do příslušných tabulek.

## Zpracování dat / transformace

Data nejsou nijak transformována, pouze historizována.

### Worker FirebasePidlitackaWorker

#### moveAll

- vstupní rabbitmq fronta
  - `dataplatform.firebasepidlitacka.moveAll`
  - bez parametrů
- závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
    1. `dataplatform.firebasepidlitacka.moveAppLaunch` bez parametrů
    2. `dataplatform.firebasepidlitacka.moveEvents` bez parametrů
    3. `dataplatform.firebasepidlitacka.moveRoute` bez parametrů
    4. `dataplatform.firebasepidlitacka.moveWebEvents`, bez parametrů

#### moveAppLaunch

- vstupní rabbitmq fronta
    - `dataplatform.firebasepidlitacka.moveAppLaunch`
    - bez parametrů
- datové zdroje
  - appLaunchDatasource -> databáze `stage` (schéma keboola) tabulka `firebase_pidlitacka_applaunch_par`
- transformace
  - žádná
- data modely
  - appLaunchModel -> (schéma public) `firebase_pidlitacka_applaunch_par`


#### moveEvents

- vstupní rabbitmq fronta
    - `dataplatform.firebasepidlitacka.moveEvents`
    - bez parametrů
- datové zdroje
    - webEventsDatasource -> databáze `stage` (schéma keboola) tabulka `firebase_pidlitacka_events`
- transformace
    - žádná
- data modely
    - webEventsModel -> (schéma public) `firebase_pidlitacka_events`

#### moveRoute

- vstupní rabbitmq fronta
    - `dataplatform.firebasepidlitacka.moveRoute`
    - bez parametrů
- datové zdroje
    - routeDatasource -> databáze `stage` (schéma keboola) tabulka `firebase_pidlitacka_route`
- transformace
    - žádná
- data modely
    - routeModel -> (schéma public) `firebase_pidlitacka_route`

#### moveWebEvents

- vstupní rabbitmq fronta
    - `dataplatform.firebasepidlitacka.moveWebEvents`
    - bez parametrů
- datové zdroje
    - webEventsDatasource -> databáze `stage` (schéma keboola) `firebase_pidlitacka_web_events`
- transformace
    - žádná
- data modely
    - webEventsModel -> (schéma public) `firebase_pidlitacka_web_events`


## Uložení dat

### Obecné

- typ databáze
  - PSQL
- databázové schéma
  ![firebase er diagram](assets/firebase_erd.png "ERD")
- retence dat
  - historizace bez promazávání
- AsyncApi dokumentace
  - [AsyncApi](./asyncapi.yaml)

## Output API

Není k dispozici.
