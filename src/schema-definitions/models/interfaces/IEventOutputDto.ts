export interface IEventOutputDto {
    event_count: number;
    event_date: string;
    event_name: string;
    users: number;
}
