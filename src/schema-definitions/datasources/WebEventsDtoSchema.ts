import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IWebEventInputDto } from "./interfaces/IWebEventInputDto";

export const webEventsDtoSchema: JSONSchemaType<IWebEventInputDto[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            id: { type: "string" },
            id_profile: { type: "string" },
            reference_date: { type: "string" },
            sessions: { type: "number" },
            users: { type: "number" },
        },
        required: ["reference_date"],
    },
};
