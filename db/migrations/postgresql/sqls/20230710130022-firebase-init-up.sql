-- firebase_pidlitacka_applaunch_par definition

CREATE TABLE firebase_pidlitacka_applaunch_par (
	event_date date NOT NULL,
	event_count int4 NOT NULL,
	users int4 NOT NULL,
	mails int4 NOT NULL,
	mail_users int4 NOT NULL,
	notis int4 NOT NULL,
	noti_users int4 NOT NULL,
	anonym int4 NOT NULL,
	anonym_users int4 NOT NULL,
	isanonym int4 NOT NULL,
	isanonym_users int4 NOT NULL,
	hasnfc int4 NOT NULL,
	hasnfc_users int4 NOT NULL,
	create_batch_id int8 NULL,
	created_at timestamp NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamp NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT firebase_pidlitacka_applaunch_par_pkey PRIMARY KEY (event_date)
);

CREATE INDEX firebase_pidlitacka_applaunch_par_create_batch ON firebase_pidlitacka_applaunch_par (create_batch_id);
CREATE INDEX firebase_pidlitacka_applaunch_par_update_batch ON firebase_pidlitacka_applaunch_par (update_batch_id);


-- firebase_pidlitacka_events definition

CREATE TABLE firebase_pidlitacka_events (
	event_date date NOT NULL,
	event_name varchar(255) NOT NULL,
	event_count int4 NOT NULL,
	users int4 NOT NULL,
	create_batch_id int8 NULL,
	created_at timestamp NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamp NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT firebase_pidlitacka_events_pkey PRIMARY KEY (event_date, event_name)
);

CREATE INDEX firebase_pidlitacka_events_create_batch ON firebase_pidlitacka_events USING btree (create_batch_id);
CREATE INDEX firebase_pidlitacka_events_update_batch ON firebase_pidlitacka_events USING btree (update_batch_id);


-- firebase_pidlitacka_route definition

CREATE TABLE firebase_pidlitacka_route (
	reference_date date NOT NULL,
	s_from varchar(255) NOT NULL,
	s_to varchar(255) NOT NULL,
	count_case int4 NOT NULL,
	create_batch_id int8 NULL,
	created_at timestamp NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamp NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT firebase_pidlitacka_route_pkey PRIMARY KEY (reference_date, s_from, s_to)
);

CREATE INDEX firebase_pidlitacka_route_create_batch ON firebase_pidlitacka_route USING btree (create_batch_id);
CREATE INDEX firebase_pidlitacka_route_update_batch ON firebase_pidlitacka_route USING btree (update_batch_id);


-- firebase_pidlitacka_web_events definition

CREATE TABLE firebase_pidlitacka_web_events (
	id varchar(255) NULL,
	id_profile varchar(255) NULL,
	reference_date date NOT NULL,
	sessions int4 NULL,
	users int4 NULL,
	create_batch_id int8 NULL,
	created_at timestamp NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamp NULL,
	updated_by varchar(150) NULL,
	CONSTRAINT firebase_pidlitacka_web_events_pkey PRIMARY KEY (reference_date)
);

CREATE INDEX firebase_pidlitacka_web_events_create_batch ON firebase_pidlitacka_web_events USING btree (create_batch_id);
CREATE INDEX firebase_pidlitacka_web_events_update_batch ON firebase_pidlitacka_web_events USING btree (update_batch_id);
