import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { IRouteInputDto } from "./interfaces/IRouteInputDto";

export const routesDtoSchema: JSONSchemaType<IRouteInputDto[]> = {
    type: "array",
    items: {
        type: "object",
        properties: {
            count_case: { type: "number" },
            reference_date: { type: "string" },
            s_from: { type: "string" },
            s_to: { type: "string" },
        },
        required: ["count_case", "reference_date", "s_from", "s_to"],
    },
};
