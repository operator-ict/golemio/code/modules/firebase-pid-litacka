export interface IEventInputDto {
    event_count: number;
    event_date: string;
    event_name: string;
    users: number;
}
