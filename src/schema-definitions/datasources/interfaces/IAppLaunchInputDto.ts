export interface IAppLaunchInputDto {
    anonym: number;
    anonym_users: number;
    event_count: number;
    event_date: string;
    hasnfc: number;
    hasnfc_users: number;
    isanonym: number;
    isanonym_users: number;
    mail_users: number;
    mails: number;
    noti_users: number;
    notis: number;
    users: number;
}
