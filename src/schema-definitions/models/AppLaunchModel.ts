import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import Sequelize, { Model, ModelAttributes, ModelIndexesOptions } from "@golemio/core/dist/shared/sequelize";
import { IAppLaunchOutputDto } from "./interfaces/IAppLaunchOutputDto";

export class AppLaunchModel extends Model<IAppLaunchOutputDto> implements IAppLaunchOutputDto {
    public static tableName = "firebase_pidlitacka_applaunch_par";

    declare anonym: number;
    declare anonym_users: number;
    declare event_count: number;
    declare event_date: string;
    declare hasnfc: number;
    declare hasnfc_users: number;
    declare isanonym: number;
    declare isanonym_users: number;
    declare mail_users: number;
    declare mails: number;
    declare noti_users: number;
    declare notis: number;
    declare users: number;

    public static attributeModel: ModelAttributes<AppLaunchModel> = {
        anonym: { type: Sequelize.INTEGER, allowNull: false },
        anonym_users: { type: Sequelize.INTEGER, allowNull: false },
        event_count: { type: Sequelize.INTEGER, allowNull: false },
        event_date: { type: Sequelize.DATE, primaryKey: true },
        hasnfc: { type: Sequelize.INTEGER, allowNull: false },
        hasnfc_users: { type: Sequelize.INTEGER, allowNull: false },
        isanonym: { type: Sequelize.INTEGER, allowNull: false },
        isanonym_users: { type: Sequelize.INTEGER, allowNull: false },
        mail_users: { type: Sequelize.INTEGER, allowNull: false },
        mails: { type: Sequelize.INTEGER, allowNull: false },
        noti_users: { type: Sequelize.INTEGER, allowNull: false },
        notis: { type: Sequelize.INTEGER, allowNull: false },
        users: { type: Sequelize.INTEGER, allowNull: false },
    };

    public static indexesOptions: ModelIndexesOptions[] = [
        {
            unique: false,
            using: "BTREE",
            name: "firebase_pidlitacka_applaunch_par_create_batch",
            fields: ["create_batch_id"],
        },
        {
            unique: false,
            using: "BTREE",
            name: "firebase_pidlitacka_applaunch_par_update_batch",
            fields: ["update_batch_id"],
        },
    ];

    public static jsonSchema: JSONSchemaType<IAppLaunchOutputDto[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                anonym: { type: "integer" },
                anonym_users: { type: "integer" },
                event_count: { type: "integer" },
                event_date: { type: "string" },
                hasnfc: { type: "integer" },
                hasnfc_users: { type: "integer" },
                isanonym: { type: "integer" },
                isanonym_users: { type: "integer" },
                mail_users: { type: "integer" },
                mails: { type: "integer" },
                noti_users: { type: "integer" },
                notis: { type: "integer" },
                users: { type: "integer" },
            },
            required: [
                "anonym",
                "anonym_users",
                "event_count",
                "event_date",
                "hasnfc",
                "hasnfc_users",
                "isanonym",
                "isanonym_users",
                "mail_users",
                "mails",
                "noti_users",
                "notis",
                "users",
            ],
        },
    };
}
