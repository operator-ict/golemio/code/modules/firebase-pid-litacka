import { FirebasePidlitackaWorker } from "#ie/FirebasePidlitackaWorker";
import { FirebasePidlitacka } from "#sch";
import { config } from "@golemio/core/dist/integration-engine/config";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { DataSourceStream } from "@golemio/core/dist/integration-engine/datasources/DataSourceStream";
import sinon, { SinonSandbox, SinonSpy } from "sinon";
import { waitTillStreamEnds } from "./helpers";

describe("FirebasePidlitackaWorker", () => {
    let worker: FirebasePidlitackaWorker;
    let sandbox: SinonSandbox;
    let sequelizeModelStub: Record<string, any>;
    let queuePrefix: string;
    let testDataAppLaunch: any[];
    let testDataEvents: any[];
    let testDataRoute: any[];
    let testDataWebEvents: any[];

    beforeEach(() => {
        const getOutputStream = async (data: any, stream: any) => {
            stream.push(data);
            stream.push(null);
            return stream;
        };

        const dataStream = new DataSourceStream({
            objectMode: true,
            read: () => {
                return;
            },
        });

        sandbox = sinon.createSandbox();
        sequelizeModelStub = Object.assign({
            hasMany: sandbox.stub(),
            removeAttribute: sandbox.stub(),
        });
        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().callsFake(() => sequelizeModelStub),
                query: sandbox.stub().callsFake(() => [true]),
                transaction: sandbox.stub().callsFake(() => Object.assign({ commit: sandbox.stub() })),
            })
        );

        testDataAppLaunch = [
            {
                anonym: 1246,
                anonym_users: 380,
                event_count: 36855,
                event_date: "2020-04-19",
                hasnfc: 12825,
                hasnfc_users: 3527,
                isanonym: 5570,
                isanonym_users: 1310,
                mail_users: 1914,
                mails: 7520,
                noti_users: 5496,
                notis: 21482,
                users: 9388,
            },
        ];
        testDataEvents = [
            {
                event_count: 10129,
                event_date: "2020-04-19",
                event_name: "notification_dismiss",
                users: 1873,
            },
            {
                event_count: 36855,
                event_date: "2020-04-19",
                event_name: "appLaunch",
                users: 9388,
            },
            {
                event_count: 15537,
                event_date: "2020-04-19",
                event_name: "connectionSearch",
                users: 5211,
            },
        ];
        testDataRoute = [
            {
                count_case: 1,
                reference_date: "2020-04-19",
                s_from: "Písková",
                s_to: "",
            },
            {
                count_case: 1,
                reference_date: "2020-04-19",
                s_from: "Můstek",
                s_to: "",
            },
            {
                count_case: 1,
                reference_date: "2020-04-19",
                s_from: "Vodičkova",
                s_to: "",
            },
        ];
        testDataWebEvents = [
            {
                id: "34948266c7ed1ab0c1e32baa47ef53a6dd05d6ff",
                id_profile: "180272358",
                reference_date: "2020-04-20",
                sessions: 715,
                users: 487,
            },
            {
                id: "bb0919745db9f0bb2daeaf797192950e1df53abd",
                id_profile: "180272358",
                reference_date: "2020-04-19",
                sessions: 3060,
                users: 1820,
            },
        ];

        worker = new FirebasePidlitackaWorker();

        sandbox
            .stub(worker["appLaunchDatasource"], "getOutputStream" as any)
            .callsFake(async () => getOutputStream(testDataAppLaunch, dataStream));
        sandbox
            .stub(worker["eventsDatasource"], "getOutputStream" as any)
            .callsFake(() => getOutputStream(testDataEvents, dataStream));
        sandbox
            .stub(worker["routeDatasource"], "getOutputStream" as any)
            .callsFake(() => getOutputStream(testDataRoute, dataStream));
        sandbox
            .stub(worker["webEventsDatasource"], "getOutputStream" as any)
            .callsFake(() => getOutputStream(testDataWebEvents, dataStream));

        sandbox.spy(worker["appLaunchDatasource"], "getAll");
        sandbox.spy(worker["eventsDatasource"], "getAll");
        sandbox.spy(worker["routeDatasource"], "getAll");
        sandbox.spy(worker["webEventsDatasource"], "getAll");

        sandbox.stub(worker["appLaunchProtocolStrategy"], "deleteData");
        sandbox.stub(worker["eventsProtocolStrategy"], "deleteData");
        sandbox.stub(worker["routeProtocolStrategy"], "deleteData");
        sandbox.stub(worker["webEventsProtocolStrategy"], "deleteData");

        sandbox.stub(worker["appLaunchModel"], "saveBySqlFunction").callsFake(async (): Promise<void> => {
            return;
        });
        sandbox.stub(worker["eventsModel"], "saveBySqlFunction");
        sandbox.stub(worker["routeModel"], "saveBySqlFunction");
        sandbox.stub(worker["webEventsModel"], "saveBySqlFunction");

        sandbox.spy(worker, "moveData" as any);

        sandbox.stub(worker, "sendMessageToExchange" as any);
        queuePrefix = config.RABBIT_EXCHANGE_NAME + "." + FirebasePidlitacka.name.toLowerCase();
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should calls the correct methods by moveAll method", async () => {
        await worker.moveAll();
        sandbox.assert.callCount(worker["sendMessageToExchange"] as SinonSpy, 4);
        ["moveAppLaunch", "moveEvents", "moveRoute", "moveWebEvents"].map((f) => {
            sandbox.assert.calledWith(
                worker["sendMessageToExchange"] as SinonSpy,
                "workers." + queuePrefix + "." + f,
                "Just do it!"
            );
        });
    });

    it("should calls the correct methods by moveAppLaunch method", async () => {
        await worker.moveAppLaunch();

        await waitTillStreamEnds(worker["dataStream"]);

        sandbox.assert.calledOnce(worker["appLaunchDatasource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["moveData"] as SinonSpy);
        sandbox.assert.calledOnce(worker["appLaunchModel"].saveBySqlFunction as SinonSpy);
        sandbox.assert.calledWith(worker["appLaunchModel"].saveBySqlFunction as SinonSpy, testDataAppLaunch);
        sandbox.assert.calledOnce(worker["appLaunchProtocolStrategy"].deleteData as SinonSpy);
        sandbox.assert.callOrder(
            worker["appLaunchDatasource"].getAll as SinonSpy,
            worker["appLaunchModel"].saveBySqlFunction as SinonSpy,
            worker["appLaunchProtocolStrategy"].deleteData as SinonSpy
        );
    });

    it("should calls the correct methods by moveEvents method", async () => {
        await worker.moveEvents();

        await waitTillStreamEnds(worker["dataStream"]);

        sandbox.assert.calledOnce(worker["eventsDatasource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["moveData"] as SinonSpy);
        sandbox.assert.calledOnce(worker["eventsModel"].saveBySqlFunction as SinonSpy);
        sandbox.assert.calledWith(worker["eventsModel"].saveBySqlFunction as SinonSpy, testDataEvents);
        sandbox.assert.calledOnce(worker["eventsProtocolStrategy"].deleteData as SinonSpy);
        sandbox.assert.callOrder(
            worker["eventsDatasource"].getAll as SinonSpy,
            worker["eventsModel"].saveBySqlFunction as SinonSpy,
            worker["eventsProtocolStrategy"].deleteData as SinonSpy
        );
    });

    it("should calls the correct methods by moveRoute method", async () => {
        await worker.moveRoute();

        await waitTillStreamEnds(worker["dataStream"]);

        sandbox.assert.calledOnce(worker["routeDatasource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["moveData"] as SinonSpy);
        sandbox.assert.calledOnce(worker["routeModel"].saveBySqlFunction as SinonSpy);
        sandbox.assert.calledWith(worker["routeModel"].saveBySqlFunction as SinonSpy, testDataRoute);
        sandbox.assert.calledOnce(worker["routeProtocolStrategy"].deleteData as SinonSpy);
        sandbox.assert.callOrder(
            worker["routeDatasource"].getAll as SinonSpy,
            worker["routeModel"].saveBySqlFunction as SinonSpy,
            worker["routeProtocolStrategy"].deleteData as SinonSpy
        );
    });

    it("should calls the correct methods by moveWebEvents method", async () => {
        await worker.moveWebEvents();

        await waitTillStreamEnds(worker["dataStream"]);

        sandbox.assert.calledOnce(worker["webEventsDatasource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["moveData"] as SinonSpy);
        sandbox.assert.calledOnce(worker["webEventsModel"].saveBySqlFunction as SinonSpy);
        sandbox.assert.calledWith(worker["webEventsModel"].saveBySqlFunction as SinonSpy, testDataWebEvents);
        sandbox.assert.calledOnce(worker["webEventsProtocolStrategy"].deleteData as SinonSpy);
        sandbox.assert.callOrder(
            worker["webEventsDatasource"].getAll as SinonSpy,
            worker["webEventsModel"].saveBySqlFunction as SinonSpy,
            worker["webEventsProtocolStrategy"].deleteData as SinonSpy
        );
    });
});
