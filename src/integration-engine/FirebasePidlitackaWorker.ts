import { FirebasePidlitacka } from "#sch";
import { appLaunchDtoSchema, eventsDtoSchema, routesDtoSchema, webEventsDtoSchema } from "#sch/datasources";
import { AppLaunchModel, EventModel, RouteModel, WebEventModel } from "#sch/models";
import { config } from "@golemio/core/dist/integration-engine/config";
import {
    DataSourceStream,
    DataSourceStreamed,
    JSONDataTypeStrategy,
    PostgresProtocolStrategyStreamed,
} from "@golemio/core/dist/integration-engine/datasources";
import { PostgresModel } from "@golemio/core/dist/integration-engine/models";
import { BaseWorker } from "@golemio/core/dist/integration-engine/workers";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";

export class FirebasePidlitackaWorker extends BaseWorker {
    private appLaunchProtocolStrategy: PostgresProtocolStrategyStreamed;
    private appLaunchDatasource: DataSourceStreamed;
    private appLaunchModel: PostgresModel;
    private dataStream!: DataSourceStream;
    private eventsProtocolStrategy: PostgresProtocolStrategyStreamed;
    private eventsDatasource: DataSourceStreamed;
    private eventsModel: PostgresModel;
    private routeProtocolStrategy: PostgresProtocolStrategyStreamed;
    private routeDatasource: DataSourceStreamed;
    private routeModel: PostgresModel;
    private webEventsProtocolStrategy: PostgresProtocolStrategyStreamed;
    private webEventsDatasource: DataSourceStreamed;
    private webEventsModel: PostgresModel;

    private queuePrefix: string;

    constructor() {
        super();
        // is it ok?
        // same DB server but different DB name
        const connectionString = config.POSTGRES_CONN?.replace(new URL(config.POSTGRES_CONN).pathname, "/stage");
        this.appLaunchProtocolStrategy = new PostgresProtocolStrategyStreamed({
            connectionString: connectionString as string,
            modelAttributes: AppLaunchModel.attributeModel,
            schemaName: "keboola",
            sequelizeAdditionalSettings: {
                timestamps: false,
                freezeTableName: true,
            },
            tableName: "firebase_pidlitacka_applaunch_par",
        });
        this.appLaunchDatasource = new DataSourceStreamed(
            FirebasePidlitacka.name + "LaunchDataSource",
            this.appLaunchProtocolStrategy,
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(FirebasePidlitacka.name + "LaunchDataSourceValidator", appLaunchDtoSchema)
        );
        this.eventsProtocolStrategy = new PostgresProtocolStrategyStreamed({
            connectionString: connectionString as string,
            modelAttributes: EventModel.attributeModel,
            schemaName: "keboola",
            sequelizeAdditionalSettings: {
                timestamps: false,
                freezeTableName: true,
            },
            tableName: "firebase_pidlitacka_events",
        });
        this.eventsDatasource = new DataSourceStreamed(
            FirebasePidlitacka.name + "EventsDataSource",
            this.eventsProtocolStrategy,
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(FirebasePidlitacka.name + "EventsDataSourceValidator", eventsDtoSchema)
        );
        this.routeProtocolStrategy = new PostgresProtocolStrategyStreamed({
            connectionString: connectionString as string,
            modelAttributes: RouteModel.attributeModel,
            schemaName: "keboola",
            sequelizeAdditionalSettings: {
                timestamps: false,
                freezeTableName: true,
            },
            tableName: "firebase_pidlitacka_route",
        });
        this.routeDatasource = new DataSourceStreamed(
            FirebasePidlitacka.name + "RoutesDataSource",
            this.routeProtocolStrategy,
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(FirebasePidlitacka.name + "RoutesDataSourceValidator", routesDtoSchema)
        );
        this.webEventsProtocolStrategy = new PostgresProtocolStrategyStreamed({
            connectionString: connectionString as string,
            modelAttributes: WebEventModel.attributeModel,
            schemaName: "keboola",
            sequelizeAdditionalSettings: {
                timestamps: false,
                freezeTableName: true,
            },
            tableName: "firebase_pidlitacka_web_events",
        });
        this.webEventsDatasource = new DataSourceStreamed(
            FirebasePidlitacka.name + "WebEventsDataSource",
            this.webEventsProtocolStrategy,
            new JSONDataTypeStrategy({ resultsPath: "" }),
            new JSONSchemaValidator(FirebasePidlitacka.name + "WebEventsDataSourceValidator", webEventsDtoSchema)
        );
        this.appLaunchModel = new PostgresModel(
            FirebasePidlitacka.name + "LaunchModel",
            {
                outputSequelizeAttributes: AppLaunchModel.attributeModel,
                pgTableName: AppLaunchModel.tableName,
                pgSchema: FirebasePidlitacka.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(FirebasePidlitacka.name + "LaunchModelValidator", AppLaunchModel.jsonSchema)
        );
        this.eventsModel = new PostgresModel(
            FirebasePidlitacka.name + "EventModel",
            {
                outputSequelizeAttributes: EventModel.attributeModel,
                pgTableName: EventModel.tableName,
                pgSchema: FirebasePidlitacka.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(FirebasePidlitacka.name + "EventModelValidator", EventModel.jsonSchema)
        );
        this.routeModel = new PostgresModel(
            FirebasePidlitacka.name + "RouteModel",
            {
                outputSequelizeAttributes: RouteModel.attributeModel,
                pgTableName: RouteModel.tableName,
                pgSchema: FirebasePidlitacka.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(FirebasePidlitacka.name + "RouteModelValidator", RouteModel.jsonSchema)
        );
        this.webEventsModel = new PostgresModel(
            FirebasePidlitacka.name + "WebEventModel",
            {
                outputSequelizeAttributes: WebEventModel.attributeModel,
                pgTableName: WebEventModel.tableName,
                pgSchema: FirebasePidlitacka.pgSchema,
                savingType: "insertOrUpdate",
            },
            new JSONSchemaValidator(FirebasePidlitacka.name + "WebEventModelValidator", WebEventModel.jsonSchema)
        );

        this.queuePrefix = config.RABBIT_EXCHANGE_NAME + "." + FirebasePidlitacka.name.toLowerCase();
    }

    public async moveAll(): Promise<void> {
        const queueNames = ["moveAppLaunch", "moveEvents", "moveRoute", "moveWebEvents"];

        await Promise.all(
            queueNames.map((queueName: string) => {
                return this.sendMessageToExchange("workers." + this.queuePrefix + "." + queueName, "Just do it!");
            })
        );
    }

    public async moveAppLaunch(): Promise<void> {
        await this.moveData(this.appLaunchDatasource, this.appLaunchModel, ["event_date"], this.appLaunchProtocolStrategy);
    }

    public async moveEvents(): Promise<void> {
        await this.moveData(this.eventsDatasource, this.eventsModel, ["event_date", "event_name"], this.eventsProtocolStrategy);
    }

    public async moveRoute(): Promise<void> {
        await this.moveData(
            this.routeDatasource,
            this.routeModel,
            ["s_from", "s_to", "reference_date"],
            this.routeProtocolStrategy
        );
    }

    public async moveWebEvents(): Promise<void> {
        await this.moveData(this.webEventsDatasource, this.webEventsModel, ["reference_date"], this.webEventsProtocolStrategy);
    }

    private async moveData(
        datasource: DataSourceStreamed,
        model: PostgresModel,
        primaryKeys: string[],
        strategy: PostgresProtocolStrategyStreamed
    ): Promise<void> {
        try {
            this.dataStream = await datasource.getAll();
        } catch (err) {
            throw new GeneralError("Error while getting data.", this.constructor.name, err);
        }

        try {
            await this.dataStream
                .setDataProcessor(async (data: any) => {
                    await model.saveBySqlFunction(data, primaryKeys);
                })
                .setOnEndFunction(async () => {
                    await strategy.deleteData();
                })
                .proceed();
        } catch (err) {
            throw new GeneralError("Error processing data.", this.constructor.name, err);
        }
    }
}
