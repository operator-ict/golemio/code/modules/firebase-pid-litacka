# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.1.11] - 2024-11-28

### Added

-   AsyncAPI documentation ([integration-engine#264](https://gitlab.com/operator-ict/golemio/code/integration-engine/-/issues/264))

## [1.1.10] - 2024-09-12

### Changed

-   `.gitlab-ci.yml` cleanup ([devops#320](https://gitlab.com/operator-ict/golemio/devops/infrastructure/-/issues/320))

## [1.1.9] - 2024-08-16

### Added

-   add backstage metadata files
-   add .gitattributes file

## [1.1.8] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [1.1.7] - 2023-08-16

-   No changelog

## [1.1.6] - 2023-07-17

### Changed

-   schema separation `public` -> `firebase_pid_litacka` ([#2](https://gitlab.com/operator-ict/golemio/code/modules/firebase-pid-litacka/-/issues/2))

## [1.1.5] - 2023-06-28

### Fixed

-   added freeze table names to Postgres datasources

## [1.1.4] - 2023-06-14

### Changed

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))

## [1.1.3] - 2023-05-31

### Changed

-   Update golemio errors ([core#62](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/62))

## [1.1.2] - 2023-03-08

### Changed

-   Convert Mongoose schema definitions to JSON schemas ([general#435](https://gitlab.com/operator-ict/golemio/code/general/-/issues/435))

## [1.1.1] - 2023-02-27

### Changed

-   Update Node.js to v18.14.0 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [1.1.0] - 2023-01-23

### Changed

-   Migrate to npm

## [1.0.5] - 2022-11-29

### Changed

-   Update gitlab-ci template

## [1.0.4] - 2022-09-21

### Removed

-   Unused dependencies ([general#5](https://gitlab.com/operator-ict/golemio/code/modules/general/-/issues/5))

## [1.0.3] - 2022-06-07

### Changed

-   Typescript version update from 4.4.4 to 4.6.4

### Added

-   Implementation docs

