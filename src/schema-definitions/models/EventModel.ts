import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import Sequelize, { Model, ModelAttributes, ModelIndexesOptions } from "@golemio/core/dist/shared/sequelize";
import { IEventOutputDto } from "./interfaces/IEventOutputDto";

export class EventModel extends Model<IEventOutputDto> implements IEventOutputDto {
    public static tableName = "firebase_pidlitacka_events";

    declare event_count: number;
    declare event_date: string;
    declare event_name: string;
    declare users: number;

    public static attributeModel: ModelAttributes<EventModel> = {
        event_count: { type: Sequelize.INTEGER, allowNull: false },
        event_date: { type: Sequelize.DATE, primaryKey: true },
        event_name: { type: Sequelize.STRING, primaryKey: true },
        users: { type: Sequelize.INTEGER, allowNull: false },
    };

    public static indexesOptions: ModelIndexesOptions[] = [
        {
            unique: false,
            using: "BTREE",
            name: "firebase_pidlitacka_events_create_batch",
            fields: ["create_batch_id"],
        },
        {
            unique: false,
            using: "BTREE",
            name: "firebase_pidlitacka_events_update_batch",
            fields: ["update_batch_id"],
        },
    ];

    public static jsonSchema: JSONSchemaType<IEventOutputDto[]> = {
        type: "array",
        items: {
            type: "object",
            properties: {
                event_count: { type: "integer" },
                event_date: { type: "string" },
                event_name: { type: "string" },
                users: { type: "integer" },
            },
            required: ["event_count", "event_date", "event_name", "users"],
        },
    };
}
