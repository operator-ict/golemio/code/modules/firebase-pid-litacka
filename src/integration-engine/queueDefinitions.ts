import { FirebasePidlitackaWorker } from "#ie/FirebasePidlitackaWorker";
import { FirebasePidlitacka } from "#sch";
import { config } from "@golemio/core/dist/integration-engine/config";
import { IQueueDefinition } from "@golemio/core/dist/integration-engine/queueprocessors";

const queueDefinitions: IQueueDefinition[] = [
    {
        name: FirebasePidlitacka.name,
        queuePrefix: config.RABBIT_EXCHANGE_NAME + "." + FirebasePidlitacka.name.toLowerCase(),
        queues: [
            {
                name: "moveAll",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 23 * 60 * 60 * 1000, // 23 hours
                },
                worker: FirebasePidlitackaWorker,
                workerMethod: "moveAll",
            },
            {
                name: "moveAppLaunch",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 23 * 60 * 60 * 1000, // 23 hours
                },
                worker: FirebasePidlitackaWorker,
                workerMethod: "moveAppLaunch",
            },
            {
                name: "moveEvents",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 23 * 60 * 60 * 1000, // 23 hours
                },
                worker: FirebasePidlitackaWorker,
                workerMethod: "moveEvents",
            },
            {
                name: "moveRoute",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 23 * 60 * 60 * 1000, // 23 hours
                },
                worker: FirebasePidlitackaWorker,
                workerMethod: "moveRoute",
            },
            {
                name: "moveWebEvents",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 23 * 60 * 60 * 1000, // 23 hours
                },
                worker: FirebasePidlitackaWorker,
                workerMethod: "moveWebEvents",
            },
        ],
    },
];

export { queueDefinitions };
